#!/usr/bin/env python

import json
import re

result = []

def main():
    # uri = input("Set uri to continue : ")
    # print(uri)

    uri = "intro.md"

    try:
        with open(uri) as file:
            # if f.endswith('.md'):
            print(read_file(file))
            # else:
            # print("File is not Markdown file")

    except IOError:
        print("File is not accessible")

def read_file(file1):
    regex = re.compile("^[A-z 0-9]")

    current_state = "neutral"

    for line in file1.readlines():

        if current_state == "neutral":

            if line.startswith("###"):
                result.append({"h3": line[4:]})

            elif line.startswith("##"):
                result.append({"h2": line[3:]})

            elif line.startswith("#"):
                result.append({"h1": line[2:]})

            elif line.startswith("```"):
                temp = {"code": line[3:]}
                current_state = "code"

            elif line.startswith("*") or line.startswith("-") or line.startswith("+"):
                temp = {"ul": [line[2:]]}
                current_state = "list"

            elif regex.match(line):
                temp = {"p": line}
                current_state = "paragraph"

        elif current_state == "code":

            if line.startswith("```"):
                result.append(temp)
                current_state = "neutral"
                temp = []

            else:
                temp["code"] += line

        elif current_state == "list":

            if line.startswith("\n"):
                result.append(temp)
                current_state = "neutral"
                temp = []

            else:
                temp["ul"].append(line)

        elif current_state == "paragraph":

            if line.startswith("\n"):
                result.append(temp)
                current_state = "neutral"
                temp = []

            else:
                temp["p"] += line

    # Ajout des données dans la variable temp lors de la fin du fichier
    if temp:
        result.append(temp)

    file1.close()

    return array_to_json_formatted(result)

    
def array_to_json_formatted(result):
    return json.dumps(result, indent=4)

main()
